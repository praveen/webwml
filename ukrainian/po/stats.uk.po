# translation of stats.po to Ukrainian
# Volodymyr Bodenchuk <Bodenchuk@bigmir.net>, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2017-11-14 23:35+0200\n"
"Last-Translator: Volodymyr Bodenchuk <Bodenchuk@bigmir.net>\n"
"Language-Team: Ukrainian <ukrainian>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms:  nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Gtranslator 2.91.7\n"

#: ../../stattrans.pl:278 ../../stattrans.pl:494
msgid "Wrong translation version"
msgstr "Невірна версія перекладу"

#: ../../stattrans.pl:280
msgid "This translation is too out of date"
msgstr "Цей переклад надто застарілий"

#: ../../stattrans.pl:282
msgid "The original is newer than this translation"
msgstr "Оригінал новіший за цей переклад"

#: ../../stattrans.pl:286 ../../stattrans.pl:494
msgid "The original no longer exists"
msgstr "Оригінал більше не існує"

#: ../../stattrans.pl:470
msgid "hits"
msgstr "звернень"

#: ../../stattrans.pl:470
msgid "hit count N/A"
msgstr "кількість звернень не відома"

#: ../../stattrans.pl:488 ../../stattrans.pl:489
msgid "Click to fetch diffstat data"
msgstr "Натисніть, щоб отримати дані diffstat"

#: ../../stattrans.pl:599 ../../stattrans.pl:739
msgid "Created with <transstatslink>"
msgstr "Створено за допомогою <transstatslink>"

#: ../../stattrans.pl:604
msgid "Translation summary for"
msgstr "Підсумок перекладу для"

#: ../../stattrans.pl:607
msgid "Translated"
msgstr "Перекладені"

#: ../../stattrans.pl:607 ../../stattrans.pl:687 ../../stattrans.pl:761
#: ../../stattrans.pl:807 ../../stattrans.pl:850
msgid "Up to date"
msgstr "Актуальні"

#: ../../stattrans.pl:607 ../../stattrans.pl:762 ../../stattrans.pl:808
msgid "Outdated"
msgstr "Застарілі"

#: ../../stattrans.pl:607 ../../stattrans.pl:763 ../../stattrans.pl:809
#: ../../stattrans.pl:852
msgid "Not translated"
msgstr "Не перекладені"

#: ../../stattrans.pl:608 ../../stattrans.pl:609 ../../stattrans.pl:610
#: ../../stattrans.pl:611
msgid "files"
msgstr "файлів"

#: ../../stattrans.pl:614 ../../stattrans.pl:615 ../../stattrans.pl:616
#: ../../stattrans.pl:617
msgid "bytes"
msgstr "байт"

#: ../../stattrans.pl:624
msgid ""
"Note: the lists of pages are sorted by popularity. Hover over the page name "
"to see the number of hits."
msgstr ""
"Примітка: цей список сторінок відсортований за популярністю. Наведіть мишку "
"на назву сторінки, щоб побачити кількість звернень."

#: ../../stattrans.pl:630
msgid "Outdated translations"
msgstr "Застарілі переклади"

#: ../../stattrans.pl:632 ../../stattrans.pl:686
msgid "File"
msgstr "Файли"

#: ../../stattrans.pl:634
msgid "Diff"
msgstr "Diff"

#: ../../stattrans.pl:636
msgid "Comment"
msgstr "Коментар"

#: ../../stattrans.pl:637
msgid "Diffstat"
msgstr "Diffstat"

#: ../../stattrans.pl:638
msgid "Git command line"
msgstr ""

#: ../../stattrans.pl:640
msgid "Log"
msgstr "Журнал"

#: ../../stattrans.pl:641
msgid "Translation"
msgstr "Переклад"

#: ../../stattrans.pl:642
msgid "Maintainer"
msgstr "Супроводжувач"

#: ../../stattrans.pl:644
msgid "Status"
msgstr "Статус"

#: ../../stattrans.pl:645
msgid "Translator"
msgstr "Перекладач"

#: ../../stattrans.pl:646
msgid "Date"
msgstr "Дата"

#: ../../stattrans.pl:653
msgid "General pages not translated"
msgstr "Не перекладені сторінки загального характеру"

#: ../../stattrans.pl:654
msgid "Untranslated general pages"
msgstr "Не перекладені сторінки загального характеру"

#: ../../stattrans.pl:659
msgid "News items not translated"
msgstr "Не перекладені сторінки новин"

#: ../../stattrans.pl:660
msgid "Untranslated news items"
msgstr "Не перекладені сторінки новин"

#: ../../stattrans.pl:665
msgid "Consultant/user pages not translated"
msgstr "Не перекладені сторінки про консультантів/користувачів"

#: ../../stattrans.pl:666
msgid "Untranslated consultant/user pages"
msgstr "Не перекладені сторінки про консультантів/користувачів"

#: ../../stattrans.pl:671
msgid "International pages not translated"
msgstr "Не перекладені міжнародні сторінки"

#: ../../stattrans.pl:672
msgid "Untranslated international pages"
msgstr "Не перекладені міжнародні сторінки"

#: ../../stattrans.pl:677
msgid "Translated pages (up-to-date)"
msgstr "Перекладені сторінки (актуальні)"

#: ../../stattrans.pl:684 ../../stattrans.pl:834
msgid "Translated templates (PO files)"
msgstr "Перекладені шаблони (файли PO)"

#: ../../stattrans.pl:685 ../../stattrans.pl:837
msgid "PO Translation Statistics"
msgstr "Статистика перекладу файлів PO"

#: ../../stattrans.pl:688 ../../stattrans.pl:851
msgid "Fuzzy"
msgstr "Не точні"

#: ../../stattrans.pl:689
msgid "Untranslated"
msgstr "Не перекладені"

#: ../../stattrans.pl:690
msgid "Total"
msgstr "Всього"

#: ../../stattrans.pl:707
msgid "Total:"
msgstr "Всього:"

#: ../../stattrans.pl:741
msgid "Translated web pages"
msgstr "Перекладені веб-сторінки"

#: ../../stattrans.pl:744
msgid "Translation Statistics by Page Count"
msgstr "Статистика перекладу за кількістю сторінок"

#: ../../stattrans.pl:759 ../../stattrans.pl:805 ../../stattrans.pl:849
msgid "Language"
msgstr "Мова"

#: ../../stattrans.pl:760 ../../stattrans.pl:806
msgid "Translations"
msgstr "Переклади"

#: ../../stattrans.pl:787
msgid "Translated web pages (by size)"
msgstr "Перекладені веб-сторінки (за розміром)"

#: ../../stattrans.pl:790
msgid "Translation Statistics by Page Size"
msgstr "Статистика перекладу за розміром сторінок"

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr "Статистика перекладу веб-сайту Debian"

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr "Всього %d сторінок для перекладу."

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr "Всього %d байт для перекладу."

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr "Всього %d рядків для перекладу."

#, fuzzy
#~| msgid "Colored diff"
#~ msgid "Commit diff"
#~ msgstr "Кольоровий diff"

#~ msgid "Colored diff"
#~ msgstr "Кольоровий diff"

#~ msgid "Unified diff"
#~ msgstr "Уніфікований diff"
