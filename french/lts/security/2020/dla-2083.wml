#use wml::debian::translation-check translation="e1b53d62f3e070fcac127a53d5c11ae93f4543ee" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait un grand nombre de déréférencements de
pointeur NULL, dûs à des valeurs de renvoi non vérifiées de <tt>malloc</tt>
et similaires dans hiredis, un client minimaliste de la bibliothèque C.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-7105">CVE-2020-7105</a>

<p>Les fonctions async.c et dict.c dans libhiredis.a dans hiredis jusqu’à 0.14.0
permettaient un déréférencement de pointeur NULL à cause de valeurs de renvoi
de <tt>malloc</tt> non vérifiées.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 0.11.0-4+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets hiredis.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2083.data"
# $Id: $
