# translation of templates.pl.po to polski
# Wojciech Zareba <wojtekz@comp.waw.pl>, 2007, 2008.
# Marcin Owsiany <porridge@debian.org>, 2013.
# Mirosław <Gabruś>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: templates.pl\n"
"PO-Revision-Date: 2013-08-15 16:45+0200\n"
"Last-Translator: Mirosław <Gabruś>\n"
"Language-Team: pl <>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Gtranslator 2.91.6\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#: ../../english/template/debian/events_common.wml:8
msgid "Upcoming Attractions"
msgstr "Nadchodzące Atrakcje"

#: ../../english/template/debian/events_common.wml:11
msgid "link may no longer be valid"
msgstr "odnośnik może nie być już aktualny"

#: ../../english/template/debian/events_common.wml:14
msgid "When"
msgstr "Kiedy"

#: ../../english/template/debian/events_common.wml:17
msgid "Where"
msgstr "Gdzie"

#: ../../english/template/debian/events_common.wml:20
msgid "More Info"
msgstr "Więcej informacji"

#: ../../english/template/debian/events_common.wml:23
msgid "Debian Involvement"
msgstr "Zaangażowanie Debiana"

#: ../../english/template/debian/events_common.wml:26
msgid "Main Coordinator"
msgstr "Główny koordynator"

#: ../../english/template/debian/events_common.wml:29
msgid "<th>Project</th><th>Coordinator</th>"
msgstr "<th>Koordynatora</th><th>Projektu</th>"

#: ../../english/template/debian/events_common.wml:32
msgid "Related Links"
msgstr "Odnośniki do powiązanych tematów"

#: ../../english/template/debian/events_common.wml:35
msgid "Latest News"
msgstr "Najnowsze wiadomości"

#: ../../english/template/debian/events_common.wml:38
msgid "Download calendar entry"
msgstr "Pobierz wpis kalendarza"

#: ../../english/template/debian/news.wml:9
msgid ""
"Back to: other <a href=\"./\">Debian news</a> || <a href=\"m4_HOME/\">Debian "
"Project homepage</a>."
msgstr ""
"Powrót do: innych <a href=\"./\">wiadomości Debiana</a> || <a href=\"m4_HOME/"
"\">strony głównej Debiana</a>."

#. '<get-var url />' is replaced by the URL and must not be translated.
#. In English the final line would look like "<http://broken.com (dead.link)>"
#: ../../english/template/debian/news.wml:17
msgid "<get-var url /> (dead link)"
msgstr "<get-var url /> (martwy odnośnik)"

#: ../../english/template/debian/projectnews/footer.wml:6
#, fuzzy
msgid ""
"To receive this newsletter in your mailbox, <a href=\"https://lists.debian."
"org/debian-news/\">subscribe to the debian-news mailing list</a>."
msgstr ""
"Aby co tydzień otrzymywać pocztą elektroniczną ten biuletyn <a href="
"\"m4_HOME/MailingLists/subscribe#debian-news\">zapisz się na listę debian-"
"news</a>."

#: ../../english/template/debian/projectnews/footer.wml:10
#: ../../english/template/debian/weeklynews/footer.wml:10
msgid "<a href=\"../../\">Back issues</a> of this newsletter are available."
msgstr "Dostępne są <a href=\"../../\">archiwalne</a> wiadomości."

#. One editor name only
#: ../../english/template/debian/projectnews/footer.wml:15
msgid ""
"<void id=\"singular\" />Debian Project News is edited by <a href=\"mailto:"
"debian-publicity@lists.debian.org\">%s</a>."
msgstr ""
"<void id=\"singular\" />Wiadomości Projektu Debian redaguje <a href=\"mailto:"
"debian-publicity@lists.debian.org\">%s</a>."

#. Two or more editors
#: ../../english/template/debian/projectnews/footer.wml:20
msgid ""
"<void id=\"plural\" />Debian Project News is edited by <a href=\"mailto:"
"debian-publicity@lists.debian.org\">%s</a>."
msgstr ""
"<void id=\"plural\" />Wiadomości Projektu Debian redagują <a href=\"mailto:"
"debian-publicity@lists.debian.org\">%s</a>."

#. One editor name only
#: ../../english/template/debian/projectnews/footer.wml:25
msgid ""
"<void id=\"singular\" />This issue of Debian Project News was edited by <a "
"href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."
msgstr ""
"<void id=\"singular\" />Ten odcinek Wiadomości Projektu Debian "
"przygotował(a) <a href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."

#. Two or more editors
#: ../../english/template/debian/projectnews/footer.wml:30
msgid ""
"<void id=\"plural\" />This issue of Debian Project News was edited by <a "
"href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."
msgstr ""
"<void id=\"plural\" />Ten odcinek Wiadomości Projektu Debian "
"przygotowali(ły) <a href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."

#. One translator only
#. One translator only
#: ../../english/template/debian/projectnews/footer.wml:35
#: ../../english/template/debian/weeklynews/footer.wml:35
msgid "<void id=\"singular\" />It was translated by %s."
msgstr "<void id=\"singular\" />Tłumaczenie: %s."

#. Two ore more translators
#. Two ore more translators
#: ../../english/template/debian/projectnews/footer.wml:40
#: ../../english/template/debian/weeklynews/footer.wml:40
msgid "<void id=\"plural\" />It was translated by %s."
msgstr "<void id=\"plural\" />Tłumaczenie: %s."

#. One female translator only
#. One female translator only
#: ../../english/template/debian/projectnews/footer.wml:45
#: ../../english/template/debian/weeklynews/footer.wml:45
msgid "<void id=\"singularfemale\" />It was translated by %s."
msgstr "<void id=\"singularfemale\" />Tłumaczenie: %s."

#. Two ore more female translators
#. Two ore more female translators
#: ../../english/template/debian/projectnews/footer.wml:50
#: ../../english/template/debian/weeklynews/footer.wml:50
msgid "<void id=\"pluralfemale\" />It was translated by %s."
msgstr "<void id=\"pluralfemale\" />Tłumaczenie: %s."

#: ../../english/template/debian/weeklynews/footer.wml:6
msgid ""
"To receive this newsletter weekly in your mailbox, <a href=\"https://lists."
"debian.org/debian-news/\">subscribe to the debian-news mailing list</a>."
msgstr ""
"Aby co tydzień otrzymywać pocztą elektroniczną ten biuletyn <a href="
"\"m4_HOME/MailingLists/subscribe#debian-news\">zapisz się na listę debian-"
"news</a>."

#. One editor name only
#: ../../english/template/debian/weeklynews/footer.wml:15
msgid ""
"<void id=\"singular\" />Debian Weekly News is edited by <a href=\"mailto:"
"dwn@debian.org\">%s</a>."
msgstr ""
"Cotygodniowe wiadomości Debiana przygotowuje <a href=\"mailto:dwn@debian.org"
"\">%s</a>."

#. Two or more editors
#: ../../english/template/debian/weeklynews/footer.wml:20
msgid ""
"<void id=\"plural\" />Debian Weekly News is edited by <a href=\"mailto:"
"dwn@debian.org\">%s</a>."
msgstr ""
"Cotygodniowe wiadomości Debiana przygotowują <a href=\"mailto:dwn@debian.org"
"\">%s</a>."

#. One editor name only
#: ../../english/template/debian/weeklynews/footer.wml:25
msgid ""
"<void id=\"singular\" />This issue of Debian Weekly News was edited by <a "
"href=\"mailto:dwn@debian.org\">%s</a>."
msgstr ""
"<void id=\"singular\" />Ten odcinek Cotygodniowych wiadomości Debiana "
"przygotował(a) <a href=\"mailto:dwn@debian.org\">%s</a>."

#. Two or more editors
#: ../../english/template/debian/weeklynews/footer.wml:30
msgid ""
"<void id=\"plural\" />This issue of Debian Weekly News was edited by <a href="
"\"mailto:dwn@debian.org\">%s</a>."
msgstr ""
"<void id=\"plural\" />Ten odcinek Cotygodniowych wiadomości Debiana "
"przygotowali(ły) <a href=\"mailto:dwn@debian.org\">%s</a>."

#: ../../english/template/debian/projectnews/boilerplates.wml:35
msgid ""
"Welcome to this year's <get-var issue /> issue of DPN, the newsletter for "
"the Debian community. Topics covered in this issue include:"
msgstr ""
"Witamy w <get-var issue /> w tym roku wydaniu DPN, biuletynie dla "
"społeczności Debiana. W tym wydaniu poruszamy następujące tematy:"

#: ../../english/template/debian/projectnews/boilerplates.wml:56
#: ../../english/template/debian/projectnews/boilerplates.wml:77
msgid ""
"According to the <a href=\"https://udd.debian.org/bugs.cgi\">Bugs Search "
"interface of the Ultimate Debian Database</a>, the upcoming release, Debian  "
"<q><get-var release /></q>, is currently affected by <get-var testing /> "
"Release-Critical bugs. Ignoring bugs which are easily solved or on the way "
"to being solved, roughly speaking, about <get-var tobefixed /> Release-"
"Critical bugs remain to be solved for the release to happen."
msgstr ""
"Według <a href=\"https://udd.debian.org/bugs.cgi\">interfejsu wyszukiwania "
"błędów bazy danych UDD</a>, zbliżającego się wydania, Debian <q><get-var "
"release /></q>, dotyczy <get-var testing /> błędów krytycznych dla wydania. "
"Nie licząc błędów łatwych do naprawienia, lub błędów które są w trakcie "
"naprawiania, około <get-var tobefixed /> błędów krytycznych pozostaje do "
"naprawienia, zanim może dojść do wydania."

#: ../../english/template/debian/projectnews/boilerplates.wml:57
msgid ""
"There are also some <a href=\"https://wiki.debian.org/ProjectNews/RC-Stats"
"\">hints on how to interpret</a> these numbers."
msgstr ""
"Patrz <a href=\"https://wiki.debian.org/ProjectNews/RC-Stats\">uwagi na "
"temat interpretacji</a> tych liczb."

#: ../../english/template/debian/projectnews/boilerplates.wml:78
msgid ""
"There are also <a href=\"<get-var url />\">more detailed statistics</a> as "
"well as some <a href=\"https://wiki.debian.org/ProjectNews/RC-Stats\">hints "
"on how to interpret</a> these numbers."
msgstr ""
"Są też dostępne <a href=\"<get-var url />\">dokładniejsze statystyki</a> "
"oraz <a href=\"https://wiki.debian.org/ProjectNews/RC-Stats\">uwagi na temat "
"interpretacji</a> tych liczb."

#: ../../english/template/debian/projectnews/boilerplates.wml:102
msgid ""
"<a href=\"<get-var link />\">Currently</a> <a href=\"m4_DEVEL/wnpp/orphaned"
"\"><get-var orphaned /> packages are orphaned</a> and <a href=\"m4_DEVEL/"
"wnpp/rfa\"><get-var rfa /> packages are up for adoption</a>: please visit "
"the complete list of <a href=\"m4_DEVEL/wnpp/help_requested\">packages which "
"need your help</a>."
msgstr ""
"<a href=\"<get-var link />\"Obecnie</a> <a href=\"m4_DEVEL/wnpp/orphaned"
"\"><get-var orphaned /> pakietów jest osieroconych</a>, a <a href=\"m4_DEVEL/"
"wnpp/rfa\"><get-var rfa /> pakietów</a> jest oferowanych do adopcji</a>: "
"zobacz kompletną listę <a href=\"m4_DEVEL/wnpp/help_requested\">pakietów "
"wymagających pomocy</a>."

#: ../../english/template/debian/projectnews/boilerplates.wml:114
msgid ""
"Please help us create this newsletter. We still need more volunteer writers "
"to watch the Debian community and report about what is going on. Please see "
"the <a href=\"https://wiki.debian.org/ProjectNews/HowToContribute"
"\">contributing page</a> to find out how to help. We're looking forward to "
"receiving your mail at <a href=\"mailto:debian-publicity@lists.debian.org"
"\">debian-publicity@lists.debian.org</a>."
msgstr ""
"Prosimy o pomoc w tworzeniu tego biuletynu. Nadal potrzebujemy więcej "
"ochotników, którzy obserwowaliby społeczność Debiana i zgłaszali "
"interesujące wydarzenia. Aby dowiedzieć się jak pomóc, zobacz <a href="
"\"https://wiki.debian.org/ProjectNews/HowToContribute\">stronę na temat "
"współpracy</a>. Z niecierpliwością oczekujemy Twojej wiadomości na <a href="
"\"mailto:debian-publicity@lists.debian.org\">debian-publicity@lists.debian."
"org</a>."

#: ../../english/template/debian/projectnews/boilerplates.wml:175
msgid ""
"Please note that these are a selection of the more important security "
"advisories of the last weeks. If you need to be kept up to date about "
"security advisories released by the Debian Security Team, please subscribe "
"to the <a href=\"<get-var url-dsa />\">security mailing list</a> (and the "
"separate <a href=\"<get-var url-bpo />\">backports list</a>, and <a href="
"\"<get-var url-stable-announce />\">stable updates list</a>) for "
"announcements."
msgstr ""
"Należy pamiętać, że jest to lista najważniejszych informacji na temat "
"bezpieczeństwa z ostatnich tygodni. Jeżeli chcesz być na bieżąco z "
"informacjami na temat bezpieczeństwa ogłaszanymi przez Zespół Bezpieczeństwa "
"Debiana zapisz się na <a href=\"<get-var url-dsa />\">listę ds. "
"bezpieczeństwa</a> (a dodatkowo na <a href=\"<get-var url-bpo />\">listę "
"backport</a>, oraz <a href=\"<get-var url-stable-announce />\">listę z "
"aktualizacjami wersji stabilnej</a>) aby otrzymywać ogłoszenia."

#: ../../english/template/debian/projectnews/boilerplates.wml:176
msgid ""
"Please note that these are a selection of the more important security "
"advisories of the last weeks. If you need to be kept up to date about "
"security advisories released by the Debian Security Team, please subscribe "
"to the <a href=\"<get-var url-dsa />\">security mailing list</a> (and the "
"separate <a href=\"<get-var url-bpo />\">backports list</a>, and <a href="
"\"<get-var url-stable-announce />\">stable updates list</a> or <a href="
"\"<get-var url-volatile-announce />\">volatile list</a>, for <q><get-var old-"
"stable /></q>, the oldstable distribution) for announcements."
msgstr ""
"Należy pamiętać, że jest to lista najważniejszych informacji na temat "
"bezpieczeństwa z ostatnich tygodni. Jeżeli chcesz być na bieżąco z "
"informacjami na temat bezpieczeństwa ogłaszanymi przez Zespół Bezpieczeństwa "
"Debiana zapisz się na <a href=\"<get-var url-dsa />\">listę ds. "
"bezpieczeństwa</a> (a dodatkowo na <a href=\"<get-var url-bpo />\">listę "
"backport</a>, oraz <a href=\"<get-var url-stable-announce />\">listę z "
"aktualizacjami wersji stabilnej</a> lub <a href=\"<get-var url-volatile-"
"announce />\">aktualizowaną listę</a>, dla <q><get-var old-stable /></q>, "
"starych stabilnych dystrybucji) aby otrzymywać ogłoszenia."

#: ../../english/template/debian/projectnews/boilerplates.wml:185
msgid ""
"Debian's Stable Release Team released an update announcement for the "
"package: "
msgstr ""
"Zespół ds. Wydania Stabilnego Debiana wydał komunikat o aktualizacji dla "
"pakietu:"

#: ../../english/template/debian/projectnews/boilerplates.wml:187
#: ../../english/template/debian/projectnews/boilerplates.wml:200
#: ../../english/template/debian/projectnews/boilerplates.wml:213
#: ../../english/template/debian/projectnews/boilerplates.wml:344
#: ../../english/template/debian/projectnews/boilerplates.wml:358
msgid ", "
msgstr ", "

#: ../../english/template/debian/projectnews/boilerplates.wml:188
#: ../../english/template/debian/projectnews/boilerplates.wml:201
#: ../../english/template/debian/projectnews/boilerplates.wml:214
#: ../../english/template/debian/projectnews/boilerplates.wml:345
#: ../../english/template/debian/projectnews/boilerplates.wml:359
msgid " and "
msgstr " i "

#: ../../english/template/debian/projectnews/boilerplates.wml:189
#: ../../english/template/debian/projectnews/boilerplates.wml:202
#: ../../english/template/debian/projectnews/boilerplates.wml:215
msgid ". "
msgstr ". "

#: ../../english/template/debian/projectnews/boilerplates.wml:189
#: ../../english/template/debian/projectnews/boilerplates.wml:202
#: ../../english/template/debian/projectnews/boilerplates.wml:215
msgid "Please read them carefully and take the proper measures."
msgstr "Proszę je uważnie przeczytać i podjąć odpowiednie środki."

#: ../../english/template/debian/projectnews/boilerplates.wml:198
msgid "Debian's Backports Team released advisories for these packages: "
msgstr "Zespół Backport Debiana wydał ostrzeżenia dla tych pakietów:"

#: ../../english/template/debian/projectnews/boilerplates.wml:211
msgid ""
"Debian's Security Team recently released advisories for these packages "
"(among others): "
msgstr ""
"Zespół Bezpieczeństwa Debiana ostatnio wydał ostrzeżenia dla następujących "
"pakietów (m.in.):"

#: ../../english/template/debian/projectnews/boilerplates.wml:240
msgid ""
"<get-var num-newpkg /> packages were added to the unstable Debian archive "
"recently."
msgstr ""
"<get-var num-newpkg /> pakiet(ów) zostało ostatnio dodanych do wersji "
"unstable Debiana."

#: ../../english/template/debian/projectnews/boilerplates.wml:242
msgid " <a href=\"<get-var url-newpkg />\">Among many others</a> are:"
msgstr " <a href=\"<get-var url-newpkg />\">Wśród wielu innych</a> są:"

#: ../../english/template/debian/projectnews/boilerplates.wml:269
msgid "There are several upcoming Debian-related events:"
msgstr "Nadchodzące wydarzenia związane z Debianem:"

#: ../../english/template/debian/projectnews/boilerplates.wml:275
msgid ""
"You can find more information about Debian-related events and talks on the "
"<a href=\"<get-var events-section />\">events section</a> of the Debian web "
"site, or subscribe to one of our events mailing lists for different regions: "
"<a href=\"<get-var events-ml-eu />\">Europe</a>, <a href=\"<get-var events-"
"ml-nl />\">Netherlands</a>, <a href=\"<get-var events-ml-ha />\">Hispanic "
"America</a>, <a href=\"<get-var events-ml-na />\">North America</a>."
msgstr ""
"Więcej informacji oraz dyskusji na temat wydarzeń związanych z Debianem "
"znajdziesz w <a href=\"<get-var events-section />\">sekcji wydarzeń</a> na "
"stronach Debiana lub zapisując się na jedną z naszych list przypisanych "
"różnym regionom: <a href=\"<get-var events-ml-eu />\">Europa</a>, <a href="
"\"<get-var events-ml-nl />\">Holandia</a>, <a href=\"<get-var events-ml-ha />"
"\">Ameryka Łacińska</a>, <a href=\"<get-var events-ml-na />\">Ameryka "
"Północna</a>."

#: ../../english/template/debian/projectnews/boilerplates.wml:300
msgid ""
"Do you want to organise a Debian booth or a Debian install party? Are you "
"aware of other upcoming Debian-related events? Have you delivered a Debian "
"talk that you want to link on our <a href=\"<get-var events-talks />\">talks "
"page</a>? Send an email to the <a href=\"<get-var events-team />\">Debian "
"Events Team</a>."
msgstr ""
"Chcesz zorganizować stoisko Debiana lub Debian install party? Znasz inne "
"nadchodzące wydarzenia związane z Debianem? Wygłosiłeś przemówienie na temat "
"Debiana i chcesz umieścić o tym link na naszej <a href=\"<get-var events-"
"talks />\">stronie o przemowach</a>? Wyślij wiadomość do <a href=\"<get-var "
"events-team />\">Zespołu Wydarzeń Debiana</a>."

#: ../../english/template/debian/projectnews/boilerplates.wml:322
msgid ""
"<get-var dd-num /> applicants have been <a href=\"<get-var dd-url />"
"\">accepted</a> as Debian Developers"
msgstr ""
"<get-var dd-num /> kandydat(ów) zostało <a href=\"<get-var dd-url />"
"\">zaakceptowanych </a> jako Deweloperzy Debiana"

#: ../../english/template/debian/projectnews/boilerplates.wml:329
msgid ""
"<get-var dm-num /> applicants have been <a href=\"<get-var dm-url />"
"\">accepted</a> as Debian Maintainers"
msgstr ""
"<get-var dm-num /> kandydat(ów) zostało <a href=\"<get-var dm-url />"
"\">zaakceptowanych</a> jako Opiekunowie Debiana"

#: ../../english/template/debian/projectnews/boilerplates.wml:336
msgid ""
"<get-var uploader-num /> people have <a href=\"<get-var uploader-url />"
"\">started to maintain packages</a>"
msgstr ""
"<get-var uploader-num /> osób(osoba) <a href=\"<get-var uploader-url />"
"\">zaczęło(a) utrzymywać pakiety</a>"

#: ../../english/template/debian/projectnews/boilerplates.wml:381
msgid ""
"<get-var eval-newcontributors-text-list /> since the previous issue of the "
"Debian Project News. Please welcome <get-var eval-newcontributors-name-list /"
"> into our project!"
msgstr ""
"<get-var eval-newcontributors-text-list /> od poprzedniego wydania "
"Aktualności Projektu Debiana. Proszę powitajmy <get-var eval-newcontributors-"
"name-list /> w naszym projekcie!"

#: ../../english/template/debian/projectnews/boilerplates.wml:394
msgid ""
"The <get-var issue-devel-news /> issue of the <a href=\"<get-var url-devel-"
"news />\">miscellaneous news for developers</a> has been released and covers "
"the following topics:"
msgstr ""
"<get-var issue-devel-news /> Publikacja o <a href=\"<get-var url-devel-news /"
">\">różnych nowościach dla deweloperów</a> została wydana i obejmuje "
"następujące zagadnienia:"

#: ../../english/News/press/press.tags:11
msgid "p<get-var page />"
msgstr "str.<get-var page />"

#: ../../english/events/talks.defs:9
msgid "Title:"
msgstr "Tytuł:"

#: ../../english/events/talks.defs:12
msgid "Author:"
msgstr "Autor:"

#: ../../english/events/talks.defs:15
msgid "Language:"
msgstr "Język:"

#: ../../english/events/talks.defs:19
msgid "Date:"
msgstr "Data:"

#: ../../english/events/talks.defs:23
msgid "Event:"
msgstr "Wydarzenie:"

#: ../../english/events/talks.defs:26
msgid "Slides:"
msgstr "Prezentacja:"

#: ../../english/events/talks.defs:29
msgid "source"
msgstr "źródło"

#: ../../english/events/talks.defs:32
msgid "PDF"
msgstr "PDF"

#: ../../english/events/talks.defs:35
msgid "HTML"
msgstr "HTML"

#: ../../english/events/talks.defs:38
msgid "MagicPoint"
msgstr "MagicPoint"

#: ../../english/events/talks.defs:41
msgid "Abstract"
msgstr "Streszczenie"

#: ../../english/News/news.rdf.in:16
msgid "Debian News"
msgstr "Wiadomości Debiana"

#: ../../english/News/news.rdf.in:19
msgid "Debian Latest News"
msgstr "Najnowsze wiadomości Debiana"

#: ../../english/News/weekly/dwn-to-rdf.pl:143
msgid "The newsletter for the Debian community"
msgstr "Biuletyn dla społeczności Debiana"

#~ msgid "Name:"
#~ msgstr "Imię i nazwisko:"

#~ msgid "Email:"
#~ msgstr "E-mail:"

#~ msgid "Previous Talks:"
#~ msgstr "Poprzednie wykłady:"

#~ msgid "Languages:"
#~ msgstr "Języki:"

#~ msgid "Location:"
#~ msgstr "Miejsce:"

#~ msgid "Topics:"
#~ msgstr "Tematy:"

#~ msgid "List of Speakers"
#~ msgstr "Lista prelegentów"

#~ msgid "Back to the <a href=\"./\">Debian speakers page</a>."
#~ msgstr "Powrót do <a href=\"./\">strony prelegentów Debian'a</a>."

#~ msgid ""
#~ "To receive this newsletter bi-weekly in your mailbox, <a href=\"https://"
#~ "lists.debian.org/debian-news/\">subscribe to the debian-news mailing "
#~ "list</a>."
#~ msgstr ""
#~ "Aby otrzymywać ten biuletyn pocztą elektroniczną w cyklu dwutygodniowym, "
#~ "<a href=\"https://lists.debian.org/debian-news/\">zapisz się na listę "
#~ "debian-news</a>."
