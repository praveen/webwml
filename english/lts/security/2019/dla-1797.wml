<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities have been discovered in drupal7, a
PHP web site platform. The vulnerabilities affect the embedded versions
of the jQuery JavaScript library and the Typo3 Phar Stream Wrapper
library.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11358">CVE-2019-11358</a>

    <p>It was discovered that the jQuery version embedded in Drupal was
    prone to a cross site scripting vulnerability in jQuery.extend().</p>

    <p>For additional information, please refer to the upstream advisory
    at <a href="https://www.drupal.org/sa-core-2019-006.">https://www.drupal.org/sa-core-2019-006.</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11831">CVE-2019-11831</a>

    <p>It was discovered that incomplete validation in a Phar processing
    library embedded in Drupal, a fully-featured content management
    framework, could result in information disclosure.</p>

    <p>For additional information, please refer to the upstream advisory
    at <a href="https://www.drupal.org/sa-core-2019-007.">https://www.drupal.org/sa-core-2019-007.</a></p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
7.32-1+deb8u17.</p>

<p>We recommend that you upgrade your drupal7 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1797.data"
# $Id: $
