<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A vulnerability was discovered in libspring-security-2.0-java, a modular
Java/J2EE application security framework, when using
SecureRandomFactoryBean#setSeed to configure a SecureRandom instance,
resulting in insecure randomness.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2.0.7.RELEASE-3+deb8u1.</p>

<p>We recommend that you upgrade your libspring-security-2.0-java packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1794.data"
# $Id: $
