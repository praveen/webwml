<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two issues have been fixed in bash, the GNU Bourne-Again Shell:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9401">CVE-2016-9401</a>

    <p>The popd builtin segfaulted when called with negative out of range
    offsets.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9924">CVE-2019-9924</a>

    <p>Sylvain Beucler discovered that it was possible to call commands
    that contained a slash when in restricted mode (rbash) by adding
    them to the BASH_CMDS array.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
4.3-11+deb8u2.</p>

<p>We recommend that you upgrade your bash packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1726.data"
# $Id: $
