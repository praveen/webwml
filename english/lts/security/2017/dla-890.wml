<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that there were multiple heap-based buffer overflows in ming,
a library to generate SWF (Flash) files.</p>

<p>The updated packages prevent a crash in the <q>listswf</q> utility due to a
heap-based buffer overflow in the parseSWF_RGBA function and several other
functions in parser.c.</p>

<p>AddressSanitizer flagged them as invalid writes <q>of size 1</q> but the heap could
be written to multiple times. The overflows are caused by a pointer behind the
bounds of a statically allocated array of structs of type SWF_GRADIENTRECORD.</p>

<p>For Debian 7 <q>Wheezy</q>, this issue has been fixed in ming version
1:0.4.4-1.1+deb7u2.</p>

<p>We recommend that you upgrade your ming packages.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-890.data"
# $Id: $
