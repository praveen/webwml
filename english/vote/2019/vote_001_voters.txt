-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    1           93sam	Steve McIntyre
    2             abe	Axel Beckert
    3        abhijith	Abhijith PA
    4           aboll	Andreas Boll
    5             adn	Mohammed Adnène Trojette
    6       adrianorg	Adriano Rafael Gomes
    7            adsb	Adam D. Barratt
    8      aerostitch	Joseph Herlant
    9             agi	Alberto Gonzalez Iniesta
   10             agx	Guido Guenther
   11              ah	Andreas Henriksson
   12          akumar	Kumar Appaiah
   13         alessio	Alessio Treglia
   14           alexm	Alex Muntada
   15           alexp	Alex Pennace
   16        alteholz	Thorsten Alteholz
   17        ametzler	Andreas Metzler
   18             ana	Ana Beatriz Guerrero López
   19         anarcat	Antoine Beaupré
   20            anbe	Andreas Beckmann
   21            andi	Andreas B. Mundt
   22           angel	Angel Abad
   23          anibal	Anibal Monsalve Salazar
   24             apo	Markus Koschany
   25         apoikos	Apollon Oikonomopoulos
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   26            ardo	Ardo van Rangelrooij
   27           asias	Asias He
   28         aurel32	Aurelien Jarno
   29       awoodland	Alan Woodland
   30              az	Alexander Zangerl
   31        azekulic	Alen Zekulic
   32        ballombe	Bill Allombert
   33           bartm	Bart Martens
   34             bas	Bas Zoetekouw
   35           bayle	Christian Bayle
   36          bbaren	Benjamin Barenblat
   37         bblough	William Blough
   38           bdale	Bdale Garbee
   39          bdrung	Benjamin Drung
   40          bengen	Hilko Bengen
   41            benh	Ben Hutchings
   42          bernat	Vincent Bernat
   43           berni	Bernhard Schmidt
   44            beuc	Sylvain Beucler
   45         bgoglin	Brice Goglin
   46           biebl	Michael Biebl
   47         bigeasy	Sebastian Andrzej Siewior
   48           blade	Eduard Bloch
   49           bluca	Luca Boccassi
   50           bmarc	Bertrand Marc
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   51           bootc	Chris Boot
   52         bottoms	Maitland Bottoms
   53          boutil	Cédric Boutillier
   54         bremner	David Bremner
   55         broonie	Mark Brown
   56            bunk	Adrian Bunk
   57         bureado	Jose Parrella
   58            bzed	Bernd Zeimetz
   59        calculus	Jerome Georges Benoit
   60        capriott	Andrea Capriotti
   61          carnil	Salvatore Bonaccorso
   62            cate	Giacomo Catenazzi
   63        cespedes	Juan Cespedes
   64       christoph	Christoph Egger
   65        cjwatson	Colin Watson
   66             ckk	Christian Kastner
   67           clint	Clint Adams
   68        codehelp	Neil Williams
   69          colint	Colin Tuckley
   70            cord	Cord Beermann
   71         coucouf	Aurélien Couderc
   72             cts	Christian T. Steigies
   73           cwryu	Changwoo Ryu
   74             dai	Daisuke Higuchi
   75           daigo	Daigo Moriwaki
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
   76          daissi	Dylan Aïssi
   77          daniel	Daniel Baumann
   78           dannf	Dann Frazier
   79           dapal	David Paleino
   80       debalance	Philipp Huebner
   81          dererk	Ulises Vitulli
   82       directhex	Jo Shields
   83       dktrkranz	Luca Falavigna
   84          dlange	Daniel Lange
   85             dmn	Damyan Ivanov
   86             dod	Dominique Dumont
   87         dogsleg	Lev Lamberov
   88         donkult	David Kalnischkies
   89        dsilvers	Daniel Silverstone
   90          ebourg	Emmanuel Bourg
   91             edd	Dirk Eddelbuettel
   92          eevans	Eric Evans
   93        ehashman	Elana Hashman
   94          elbrus	Paul Mathijs Gevers
   95          enrico	Enrico Zini
   96         epsilon	Daniel Echeverry
   97            eric	Eric Dorland
   98           eriks	Erik Schanze
   99           eriol	Daniele Tricoli
  100           eugen	Eugeniy Meshcheryakov
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  101          eugene	Eugene Zhukov
  102             evo	Davide Puricelli
  103           fabbe	Fabian Fagerholm
  104          fabian	Fabian Greffrath
  105             faw	Felipe Augusto van de Wiel
  106          fgeyer	Felix Geyer
  107         filippo	Filippo Giunchedi
  108             flo	Florian Lohoff
  109         florian	Florian Ernst
  110         fpeters	Frederic Peters
  111        francois	Francois Marier
  112           fuddl	Bruno Kleinert
  113         genannt	Jonas Genannt
  114        georgesk	Georges Khaznadar
  115           ghedo	Alessandro Ghedini
  116          ginggs	Graham Inggs
  117             gio	Giovanni Mascellani
  118         giovani	Giovani Augusto Ferreira
  119           gladk	Anton Gladky
  120        glaubitz	John Paul Adrian Glaubitz
  121          glaweh	Henning Glawe
  122          glondu	Stéphane Glondu
  123          gniibe	NIIBE Yutaka
  124         godisch	Martin A. Godisch
  125          gregoa	Gregor Herrmann
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  126         guilhem	Guilhem Moulin
  127         guillem	Guillem Jover
  128          gusnan	Andreas Rönnquist
  129            guus	Guus Sliepen
  130           gwolf	Gunnar Wolf
  131        harmoney	Patty Langasek
  132        hartmans	Sam Hartman
  133           hefee	Sandro Knauß
  134         helmutg	Helmut Grohne
  135         henrich	Hideki Yamane
  136         hertzog	Raphaël Hertzog
  137             hle	Hugo Lefeuvre
  138      hlieberman	Harlan Lieberman-Berg
  139             hmh	Henrique de Moraes Holschuh
  140         hoexter	Sven Hoexter
  141          holger	Holger Levsen
  142      hvhaugwitz	Hannes von Haugwitz
  143        iliastsi	Ilias Tsitsimpis
  144       intrigeri	Intrigeri
  145             irl	Iain R. Learmonth
  146        ishikawa	Ishikawa Mutsumi
  147          iustin	Iustin Pop
  148           ivodd	Ivo De Decker
  149        iwamatsu	Nobuhiro Iwamatsu
  150          jackyf	Eugene V. Lyubimkin
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  151             jak	Julian Andres Klode
  152             jan	Jan Niehusmann
  153           jandd	Jan Dittberner
  154          jaqque	John Robinson
  155         jbfavre	Jean Baptiste Favre
  156             jcc	Jonathan Cristopher Carter
  157        jcristau	Julien Cristau
  158             jdg	Julian Gilbey
  159             jeb	James Bromberger
  160         jeffity	Lisa Baron
  161          jelmer	Jelmer Vernooij
  162           jimmy	Jimmy Kaplowitz
  163             jjr	Jeffrey Ratcliffe
  164          jlines	John Lines
  165         jmarler	Jonathan Marler
  166             jmm	Moritz Muehlenhoff
  167             jmw	Jonathan Wiltshire
  168           joerg	Joerg Jaspert
  169         joostvb	Joost van Baal
  170           jordi	Jordi Mallach
  171           josue	Josué Ortega
  172             joy	Josip Rodin
  173          jpuydt	Julien Puydt
  174          jrtc27	James Clarke
  175              js	Jonas Smedegaard
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  176        jspricke	Jochen Sprickerhof
  177         kaction	Dmitry Bogatov
  178            kaol	Kari Pahula
  179          kartik	Kartik Mistry
  180             kcr	Karl Ramm
  181          keithp	Keith Packard
  182          khalid	Khalid Aziz
  183            kibi	Cyril Brulebois
  184       kitterman	Scott Kitterman
  185            klee	Klee Dienes
  186            knok	Takatsugu Nokubi
  187           krait	Christopher Knadle
  188         kreckel	Richard Kreckel
  189      kritzefitz	Sven Bartscher
  190            kula	Marcin Kulisz
  191           lamby	Chris Lamb
  192           laney	Iain Lane
  193           lange	Thomas Lange
  194         larjona	Laura Arjona Reina
  195        lavamind	Jerome Charaoui
  196        lawrencc	Christopher Lawrence
  197         ldrolez	Ludovic Drolez
  198     leatherface	Julien Patriarca
  199         legoktm	Kunal Mehta
  200             leo	Carsten Leonhardt
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  201        lfilipoz	Luca Filipozzi
  202        lightsey	John Lightsey
  203        lisandro	Lisandro Damián Nicanor Pérez Meyer
  204          lkajan	Laszlo Kajan
  205         lmamane	Lionel Elie Mamane
  206   locutusofborg	Gianfranco Costamagna
  207         lopippo	Filippo Rusconi
  208           lucas	Lucas Nussbaum
  209             luk	Luk Claes
  210       madamezou	Francesca Ciceri
  211         madduck	Martin F. Krafft
  212         madkiss	Martin Loschwitz
  213            mafm	Manuel A. Fernandez Montecelo
  214            maks	Maximilian Attems
  215            manu	Emmanuel Kasper Kasprzyk
  216           marga	Margarita Manterola
  217         matthew	Matthew Vernon
  218     matthieucan	Matthieu Caneill
  219        matthijs	Matthijs Möhlmann
  220          mattia	Mattia Rizzolo
  221            maxx	Martin Wuertele
  222            maxy	Maximiliano Curia
  223          mbanck	Michael Banck
  224         mbehrle	Mathias Behrle
  225           mbuck	Martin Buck
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  226       mckinstry	Alastair McKinstry
  227              md	Marco d'Itri
  228       mechtilde	Mechtilde Stehmann
  229            mejo	Jonas Meurer
  230          merker	Karsten Merker
  231          meskes	Michael Meskes
  232           metal	Marcelo Jorge Vieira
  233             mfv	Matteo F. Vescovi
  234        mgilbert	Michael Gilbert
  235           micha	Micha Lenk
  236             mih	Michael Hanke
  237            mika	Michael Prokop
  238           milan	Milan Kupcevic
  239        mjeanson	Michael Jeanson
  240         moeller	Steffen Möller
  241           mones	Ricardo Mones Lastra
  242           moray	Moray Allan
  243           mpitt	Martin Pitt
  244              mt	Michael Tautschnig
  245     mtecknology	Michael Lustfield
  246        mtmiller	Mike Miller
  247           murat	Murat Demirten
  248            myon	Christoph Berg
  249          nattie	Nattie Mayer-Hutchings
  250    natureshadow	Dominik George
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  251          nbreen	Nicholas Breen
  252           neilm	Neil McGovern
  253           nijel	Michal Cihar
  254           noahm	Noah Meyerhans
  255         nomeata	Joachim Breitner
  256         noodles	Jonathan McDowell
  257           ntyni	Niko Tyni
  258            odyx	Didier Raboud
  259           olasd	Nicolas Dandrimont
  260         olebole	Ole Streicher
  261           osamu	Osamu Aoki
  262            otto	Otto Kekäläinen
  263            pabs	Paul Wise
  264        paravoid	Faidon Liambotis
  265         paulliu	Ying-Chun Liu
  266             peb	Pierre-Elliott Bécue
  267        pfaffben	Ben Pfaff
  268           philh	Philip Hands
  269            phls	Paulo Henrique de Lima Santana
  270            pini	Gilles Filippini
  271           piotr	Piotr Ożarowski
  272             pjb	Phil Brooke
  273           pkern	Philipp Kern
  274          plessy	Charles Plessy
  275       pmatthaei	Patrick Matthäi
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  276           pollo	Louis-Philippe Véronneau
  277        porridge	Marcin Owsiany
  278         praveen	Praveen Arimbrathodiyil
  279        preining	Norbert Preining
  280        pvaneynd	Peter Van Eynde
  281          rafael	Rafael Laboissiere
  282             reg	Gregory Colpart
  283         reichel	Joachim Reichel
  284            rene	Rene Engelhard
  285      rfrancoise	Romain Francoise
  286          rhonda	Rhonda D'Vine
  287             rlb	Rob Browning
  288         roberto	Roberto C. Sanchez
  289          roland	Roland Rosenfeld
  290            rosh	Roger Shimizu
  291        rousseau	Ludovic Rousseau
  292             rra	Russ Allbery
  293             rrs	Ritesh Raj Sarraf
  294     rvandegrift	Ross Vandegrift
  295        santiago	Santiago Ruano Rincón
  296         sanvila	Santiago Vila
  297         sathieu	Mathieu Parent
  298           satta	Sascha Steinbiss
  299          sbadia	Sebastien Badia
  300        schultmc	Michael C. Schultheiss
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  301         seamlik	Kai-Chung Yan
  302             seb	Sebastien Delafond
  303       sebastien	Sébastien Villemot
  304           segre	Carlo U. Segre
  305        sergiodj	Sergio Durigan Junior
  306         serpent	Tomasz Rybak
  307              sf	Stefan Fritsch
  308          sfrost	Stephen Frost
  309        siretart	Reinhard Tartler
  310            smcv	Simon McVittie
  311       spwhitton	Sean Whitton
  312       sramacher	Sebastian Ramacher
  313             sre	Sebastian Reichel
  314          ssgelm	Stephen Gelman
  315      stapelberg	Michael Stapelberg
  316        stappers	Geert Stappers
  317          steele	David Steele
  318         stevenc	Steven Chamberlain
  319       sthibault	Samuel Thibault
  320          stuart	Stuart Prescott
  321            sune	Sune Vuorela
  322       sunweaver	Mike Gabriel
  323            tach	Taku Yasui
  324          taffit	David Prévot
  325          takaki	Takaki Taniguchi
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  326          tassia	Tássia Camões Araújo
  327             tbm	Martin Michlmayr
  328        terceiro	Antonio Terceiro
  329          tfheen	Tollef Fog Heen
  330              tg	Thorsten Glaser
  331         thansen	Tobias Hansen
  332             thb	Thaddeus H. Black
  333           thijs	Thijs Kinkhorst
  334             thk	Thomas Koch
  335           tiago	Tiago Bortoletto Vaz
  336          tianon	Tianon Gravi
  337          tijuca	Carsten Schoenert
  338           tille	Andreas Tille
  339             tin	Agustin Henze
  340        tjhukkan	Teemu Hukkanen
  341        tmancill	Tony Mancill
  342            tobi	Tobias Frost
  343           toddy	Tobias Quathamer
  344            toni	Toni Mueller
  345         treinen	Ralf Treinen
  346        tsimonq2	Simon Quigley
  347        tvainika	Tommi Vainikainen
  348        tvincent	Thomas Vincent
  349           tytso	Theodore Y. Ts'o
  350         tzafrir	Tzafrir Cohen
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  351            ucko	Aaron M. Ucko
  352        ukleinek	Uwe Kleine-König
  353          ulrike	Ulrike Uhlig
  354         unit193	Unit 193  
  355         vagrant	Vagrant Cascadian
  356        valhalla	Elena Grandi
  357          vcheng	Vincent Cheng
  358           viiru	Arto Jantunen
  359           vince	Vincent Sanders
  360          vorlon	Steve Langasek
  361           vseva	Victor Seva
  362          vvidic	Valentin Vidic
  363          wagner	Hanno Wagner
  364          weasel	Peter Palfrader
  365        weinholt	Göran Weinholt
  366          wijnen	Bas Wijnen
  367           willi	Willi Mann
  368          wouter	Wouter Verhelst
  369            wrar	Andrey Rahmatullin
  370             xam	Max Vozeler
  371          xluthi	Xavier Lüthi
  372            xnox	Dimitri John Ledkov
  373            yadd	Xavier Guimard
  374            zack	Stefano Zacchiroli
  375            zeha	Christian Hofstaedtler
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
Count     Debian UID 	   Name 
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  376            zhsj	Shengjing Zhu
  377            zigo	Thomas Goirand
  378           zumbi	Hector Oron
